package com.attend.app;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.RegionBootstrap;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.json.JSONException;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kinvey.android.*;
import com.kinvey.java.User;
import com.kinvey.java.core.KinveyClientCallback;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import im.delight.android.ddp.db.memory.InMemoryDatabase;

public class BeaconReferenceApplication extends Application implements BootstrapNotifier, MeteorCallback {
    Meteor meteor;

    private static final String TAG = "BeaconReferenceApp";
    private RegionBootstrap regionBootstrap;
    private BackgroundPowerSaver backgroundPowerSaver;
    private boolean haveDetectedBeaconsSinceBoot = false;
    private MonitoringActivity monitoringActivity = null;
    private long time = 0;

    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        SharedPreferences userData = getSharedPreferences("data", 0);
        String username = userData.getString(DataHandling.SAVE_USERNAME_ID, "null");
        String password = userData.getString(DataHandling.SAVE_PASSWORD_ID, "null");
        MeteorSingleton.createInstance(this, "wss://withattendweb-65560.onmodulus.net/websocket");
        if(isInternetWorking()) {
            // create a new instance
            meteor = MeteorSingleton.getInstance();
            meteor.setLoggingEnabled(true);
            // register the callback that will handle events and receive messages
            meteor.addCallback(this);
            // establish the connection
            if(!meteor.isConnected()) {
                meteor.connect();
            }
            else {
                if (!meteor.isLoggedIn()) {
                    try {
                        login();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else {
            Toast.makeText(BeaconReferenceApplication.this, "Please make sure you are connected to internet", Toast.LENGTH_LONG).show();
        }
        BeaconManager beaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().clear();
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        Log.d(TAG, "setting up background monitoring for beacons and power saving");
        // wake up the app when a beacon is seen
        //this determines which beacons the app looks for?
        Region region = new Region("backgroundRegion",Identifier.parse("f7826da6-4fa2-4e98-8024-bc5b71e0893e"),Identifier.parse("20600"),Identifier.parse("5666"));
        //Region region = new Region("backgroundRegion","f7826da6-4fa2-4e98-8024-bc5b71e0893e","20600","5666");
        regionBootstrap = new RegionBootstrap(this, region);

        // simply constructing this class and holding a reference to it in your custom Application
        // class will automatically cause the BeaconLibrary to save battery whenever the application
        // is not visible.  This reduces bluetooth power usage by about 60%
        backgroundPowerSaver = new BackgroundPowerSaver(this);
    }

    public void login() throws JSONException, IOException {
        //check password and email here
        SharedPreferences userData = getSharedPreferences("data", 0);
        String password = userData.getString(DataHandling.SAVE_PASSWORD_ID,"null");
        String username = userData.getString(DataHandling.SAVE_USERNAME_ID,"null");
        System.out.println("Logging in with username: "+username+" Password: "+password);
        System.out.println("Connecting to server");
        if (isInternetWorking()) {
            System.out.println("Internet is working");
            meteor.loginWithUsername(username, password, new ResultListener() {
                @Override
                public void onSuccess(String result) {
                    System.out.print("Logged in");
                }
                @Override
                public void onError(String error, String reason, String details) {
                    System.out.print("Login error: "+error);
                }
            });
        }else {
            System.out.println("Internet is not working");
        }
    }

    @Override
    public void didEnterRegion(Region region) {
        if(isInternetWorking()) {
            if (meteor.isLoggedIn()) {
                System.out.print("did enter region.");
                SharedPreferences userData = getSharedPreferences("data", 0);
                final SharedPreferences.Editor editor = userData.edit();
                editor.putBoolean("inRange", true);
                editor.commit();
                //Date collection may not function correctly
                Calendar c = Calendar.getInstance();
                SimpleDateFormat dateformat = new SimpleDateFormat("MMM dd, yyyy, h:mm a");
                final String dateString = dateformat.format(c.getTime());
                System.out.println(dateString);
                final Map<String, Object> json = new HashMap<>();
                System.out.println("Logging entry for user: " + userData.getString("userName", "null"));
                json.put("time", dateString);
                json.put("userId", userData.getString(DataHandling.SAVE_USER_ID,"null"));
                json.put("beaconMinor", userData.getString(DataHandling.SAVE_LAST_FOUND_BEACON_MINOR,"null"));
                json.put("beaconMajor", userData.getString(DataHandling.SAVE_LAST_FOUND_BEACON_MAJOR,"null"));
                json.put("companyId", userData.getString(DataHandling.SAVE_COMPANY_ID,"null"));
                final Object[] log = {json};
                meteor.call("passiveEvents.enterSite", log, new ResultListener() {
                    @Override
                    public void onSuccess(String result) {
                        System.out.println("Logged Entered region in background");
                        editor.putBoolean("checkedIn", true);
                        editor.commit();
                    }
                    @Override
                    public void onError(String error, String reason, String details) {
                        System.out.println("Failed to save log in region: " + error + "Reason: " + reason + " Details: " + details);
                        //try to save on device
                        String[] data = {"Jun 27, 2016, 12:33 AM", "1", "1", "1", "1", "1"};
                        storeLogOnDevice(data);
                    }
                });
            } else {
                //store on device
                String[] data = {"Jun 27, 2016, 12:33 AM", "1", "1", "1", "1", "1"};
                storeLogOnDevice(data);
            }
        }else{
            String[] data = {"Jun 27, 2016, 12:33 AM", "1", "1", "1", "1", "1"};
            storeLogOnDevice(data);
        }
    }

    @Override
    public void didExitRegion(Region region) {
        if (isInternetWorking()) {
            if (meteor.isLoggedIn()) {
                System.out.print("did exit region.");
                SharedPreferences userData = getSharedPreferences("data", 0);
                final SharedPreferences.Editor editor = userData.edit();
                editor.putBoolean("inRange", false);
                editor.commit();
                long date = System.currentTimeMillis();
                //Date collection may not function correctly
                Calendar c = Calendar.getInstance();
                SimpleDateFormat dateformat = new SimpleDateFormat("MMM dd, yyyy, h:mm a");
                final String dateString = dateformat.format(c.getTime());
                System.out.println(dateString);
                final Map<String, Object> json = new HashMap<>();
                System.out.println("Logging exit for user: " + userData.getString(DataHandling.SAVE_USERNAME_ID, "null"));
                json.put("time", dateString); //fudged date for simplicity atm
                json.put("userId", userData.getString(DataHandling.SAVE_USER_ID,"null"));
                json.put("companyId",userData.getString(DataHandling.SAVE_COMPANY_ID,"null"));
                json.put("beaconMinor", userData.getString(DataHandling.SAVE_LAST_FOUND_BEACON_MINOR,"null"));
                json.put("beaconMajor", userData.getString(DataHandling.SAVE_LAST_FOUND_BEACON_MAJOR,"null"));
                json.put("eventType", 0);
                final Object[] log = {json};
                meteor.call("passiveEvents.leaveSite", log, new ResultListener() {
                    @Override
                    public void onSuccess(String result) {
                        System.out.println("Logged Exit region in background");
                        editor.putBoolean("checkedIn", true);
                        editor.commit();
                    }
                    @Override
                    public void onError(String error, String reason, String details) {
                        System.out.println("Failed to save log in region: " + error + "Reason: " + reason + " Details: " + details);
                        //try to save on device
                        String[] data = {"Jun 27, 2016, 12:33 AM", "1", "1", "1", "1", "0"};
                        storeLogOnDevice(data);
                    }
                });
            } else {
                //store on device
                String[] data = {"Jun 27, 2016, 12:33 AM", "1", "1", "1", "1", "0"};
                storeLogOnDevice(data);
            }
        }else{
            String[] data = {"Jun 27, 2016, 12:33 AM", "1", "1", "1", "1", "0"};
            storeLogOnDevice(data);
        }
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        if (monitoringActivity != null) {
            monitoringActivity.logToDisplay("I have just switched from seeing/not seeing beacons: " + state);
        }
    }

    private void sendNotification() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                        .setContentTitle("Attend")
                        .setContentText("You are in the beacon zone and can now checkin.")
                        .setSmallIcon(R.drawable.attendlogo)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setSound(notification);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(new Intent(this, RangingActivity.class));
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

    public void setMonitoringActivity(MonitoringActivity activity) {
        this.monitoringActivity = activity;
    }

    public boolean isInternetWorking() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void storeLogOnDevice(String[] log){
        /*SharedPreferences userData = getSharedPreferences("data", 0);
        final SharedPreferences.Editor editor = userData.edit();
        String savedlog = log[0];
        for(int i = 1; i < log.length; i++){
            savedlog += "|"+log[i];
        }
        System.out.println("Storing data"+savedlog);


        int logs = userData.getInt(DataHandling.SAVED_DATA_BACKGROUND_AMOUNT,0);
        editor.putString(DataHandling.SAVED_DATA_BACKGROUND_ID+logs,savedlog);
        editor.putInt(DataHandling.SAVED_DATA_BACKGROUND_AMOUNT, logs+1);
        editor.commit();
        if(Integer.parseInt(log[5]) == 1){
            editor.putBoolean("checkedIn", true);
        }
        if(Integer.parseInt(log[5]) == 0){
            editor.putBoolean("checkedIn", false);
        }
        editor.putBoolean("checkedIn", false);*/
    }

    public void uploadLogsStoredOnDevice(){
        SharedPreferences userData = getSharedPreferences("data", 0);
        final SharedPreferences.Editor editor = userData.edit();
        int logs = userData.getInt(DataHandling.SAVED_DATA_BACKGROUND_ID,0);
        String data[] = new String[logs];
        for(int i = 0; i < logs; i++)
        {
            data[i] = userData.getString(DataHandling.SAVED_DATA_BACKGROUND_ID,"error");
            String parseData[] = data[i].split("|");
            final Map<String, Object> json = new HashMap<>();
            json.put("time", parseData[0]);
            json.put("userId", parseData[1]);
            json.put("companyId", parseData[2]);
            json.put("beaconMinor", parseData[3]);
            json.put("beaconMajor", parseData[4]);
            json.put("eventType", Integer.parseInt(parseData[5]));
            final Object[] log = {json};
            if(Integer.parseInt(parseData[5]) == 1) {
                if (meteor.isLoggedIn()) {
                    meteor.call("passiveEvents.enterSite", log, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {
                            System.out.println("Saved log out");
                        }

                        @Override
                        public void onError(String error, String reason, String details) {
                            System.out.println("Failed to upload log: " + error + "Reason: " + reason + " Details: " + details);
                        }
                    });
                } else {
                    //do nothing
                }
            }
            else{
                if (meteor.isLoggedIn()) {
                    meteor.call("passiveEvents.leaveSite", log, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {
                            System.out.println("Saved log out");
                        }

                        @Override
                        public void onError(String error, String reason, String details) {
                            System.out.println("Failed to upload log: " + error + "Reason: " + reason + " Details: " + details);
                        }
                    });
                } else {
                    //do nothing
                }
            }
        }
    }

    @Override
    public void onConnect(boolean signedInAutomatically) {

    }

    @Override
    public void onDisconnect() {

    }

    @Override
    public void onException(Exception e) {

    }

    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {

    }

    @Override
    public void onDataChanged(String collectionName, String documentID, String updatedValuesJson, String removedValuesJson) {

    }

    @Override
    public void onDataRemoved(String collectionName, String documentID) {

    }

}