package com.attend.app;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;
import com.kinvey.java.model.KinveyMetaData;

/**
 * Created by work on 16-04-20.
 */
public class siteLog extends GenericJson {
    @Key
    public String username;
    @Key
    public String date;
    @Key
    public Boolean onsite;
    @Key
    public String location;
    @Key
    public String name;
    @Key
    public String platform;
    @Key("_kmd")
    private KinveyMetaData meta; // Kinvey metadata, OPTIONAL
    @Key("_acl")
    private KinveyMetaData.AccessControlList acl;
    public siteLog(){} //GenericJson classes must have a public empty constructor
}