package com.attend.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;

import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;

/**
 * Created by work on 16-04-23.
 */
public class splashActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_ranging);
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        FirebaseInstanceId.getInstance().getToken();
        System.out.println("TOKEN: "+FirebaseInstanceId.getInstance().getToken());
        SharedPreferences userData = getSharedPreferences("data", 0);
        if (userData.getBoolean(DataHandling.SAVE_USER_AUTHENTICATED, false)) {
            Intent intent = new Intent(this, RangingActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private boolean isInternetWorking() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
