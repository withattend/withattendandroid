package com.attend.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.IOException;
import java.io.Serializable;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.MeteorSingleton;
import im.delight.android.ddp.ResultListener;
import im.delight.android.ddp.db.memory.InMemoryDatabase;

public class LoginActivity extends Activity implements MeteorCallback, Serializable {
    MonitoringActivity monitoringActivity = null;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    protected static final String TAG = "LoginActivity";
    Meteor meteor;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // create a new instance
        meteor = MeteorSingleton.getInstance();
        // register the callback that will handle events and receive messages
        meteor.setLoggingEnabled(true);
        meteor.addCallback(this);
        // establish the connection
        if(!meteor.isConnected()) {
            meteor.connect();
        }
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            //AndroidMPermissioncheck 
            if(this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_login_screen);
        final Button loginButton = (Button) findViewById(R.id.beginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    System.out.println("attempting log in");
                    attemptLogin();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void attemptLogin() throws JSONException, IOException {
        //check password and email here
        final ProgressDialog progress = ProgressDialog.show(this, "Logging in", "Please wait", true);
        final Thread t = new Thread() {
            @Override
            public void run() {
                SharedPreferences userData = getSharedPreferences("data", 0);
                String password = userData.getString(DataHandling.SAVE_PASSWORD_ID,"null");
                String username = userData.getString(DataHandling.SAVE_USERNAME_ID,"null");
                System.out.println("Logging in with username: "+username+" Password: "+password);
                System.out.println("Connecting to server");
                if (isInternetWorking()) {
                    System.out.println("Internet is working");
                    meteor.loginWithUsername(username, password, new ResultListener() {
                        @Override
                        public void onSuccess(String result) {
                            System.out.print("Logged in");
                            progress.dismiss();
                            logUser();
                            SegueToScan();

                        }
                        @Override
                        public void onError(String error, String reason, String details) {
                            System.out.print("Login error: "+error);
                            progress.dismiss();
                        }
                    });
                }else {
                    System.out.println("Internet is not working");
                    progress.dismiss();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(LoginActivity.this, "Please make sure you are connected to the internet", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        };
        t.start();
    }
    public void SegueToScan() {
        startActivity(new Intent(this, RangingActivity.class));
    }

    private boolean isInternetWorking() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    public void onConnect(boolean signedInAutomatically) {

    }

    @Override
    public void onDisconnect() {

    }

    @Override
    public void onException(Exception e) {

    }

    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {

    }

    @Override
    public void onDataChanged(String collectionName, String documentID, String updatedValuesJson, String removedValuesJson) {

    }

    @Override
    public void onDataRemoved(String collectionName, String documentID) {

    }

    public void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        final SharedPreferences userData = getSharedPreferences("data", 0);
        Crashlytics.setUserIdentifier(userData.getString(DataHandling.SAVE_USER_ID,"null"));
        Crashlytics.setUserName("User");
    }
}
